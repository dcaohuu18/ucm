import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.utils.validation import check_array, check_is_fitted
from sklearn.pipeline import Pipeline


class PseudoLabeling(TransformerMixin, BaseEstimator):
    def __init__(self, classes, certain_thresh=0.51):
        self.real_classes_ = np.array(classes)
        self.class_unknown_ = min(self.real_classes_) - 1
        self.pseudo_classes_ = np.append(self.real_classes_, self.class_unknown_) 
        self.certain_thresh = certain_thresh

    def fit(self, X, y=None):
        X = check_array(X, accept_sparse=True)
        
        self.n_classes_ = X.shape[1]
        if self.n_classes_ != len(self.real_classes_):
            raise ValueError('Shape of input is not consistent '
                             'with number of classes given')
        
        return self

    def transform(self, X):
        # Check is fit had been called
        check_is_fitted(self, 'n_classes_')

        # Input validation
        X = check_array(X, accept_sparse=True)

        # Check that the input is of the same shape as the one passed
        # during fit.
        if X.shape[1] != self.n_classes_:
            raise ValueError('Shape of input is different from what was seen'
                             'in `fit`')
        
        thresh_col = np.full((X.shape[0], 1), self.certain_thresh)
        X = np.append(X, thresh_col, axis=1)

        return self.pseudo_classes_[np.argmax(X, axis=1)]


if __name__ == '__main__':
    pl = PseudoLabeling(classes = [0, 1, 2], certain_thresh = 0.6)
    X = np.array([[0.2, 0.2, 0.6],
                  [0.5, 0.4, 0.1],
                  [0.25, 0.7, 0.05]])
    print(pl.fit_transform(X))