import numpy as np
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from sklearn.utils.multiclass import unique_labels


class MajorityVoting(ClassifierMixin, BaseEstimator):
    def __init__(self, estimators):
        self.estimators = estimators

    def fit(self, X, y):
        # Check that X and y have correct shape
        X, y = check_X_y(X, y)
        self.X_ = X
        self.y_ = y
        
        # Store the classes seen during fit
        self.classes_ = unique_labels(y)
        
        # Fit each base estimator
        for e in self.estimators:
            e.fit(self.X_, self.y_)
            
        return self

    def predict_proba(self, X):
        # Check is fit had been called
        check_is_fitted(self, ['X_', 'y_'])

        # Input validation
        X = check_array(X)
        
        # raw_predictions = (number of estimators) x (number of instances) 
        raw_predictions = np.array([e.predict(X) for e in self.estimators])
        
        # prob_predictions = (number of instances) x (number of classes) 
        self.prob_predictions = np.empty(shape=[len(X), len(self.classes_)])
        for i, c in enumerate(self.classes_):
            self.prob_predictions[:, i] = np.count_nonzero(raw_predictions==c, axis=0)/len(self.estimators)
            
        return self.prob_predictions

    def predict(self, X):
        return self.classes_[np.argmax(self.predict_proba(X), axis=1)]


if __name__ == '__main__':
    from sklearn.linear_model import LogisticRegression
    from sklearn.naive_bayes import GaussianNB
    from sklearn.ensemble import RandomForestClassifier
    
    X = np.array([[-1, -1], [-2, -1], [-3, -2], [1, 1], [2, 1], [3, 2]])
    y = np.array([1, 0, 1, 2, 2, 2])

    clf1 = LogisticRegression(solver='lbfgs', multi_class = 'auto')
    clf2 = RandomForestClassifier(n_estimators=50, random_state=1)
    clf3 = GaussianNB()

    my_major_voting = MajorityVoting(estimators = [clf1, clf2, clf3])
    my_major_voting.fit(X, y)
    pred_arr = my_major_voting.predict_proba(X)
    print(pred_arr)