# Unlabeled Consensus Modeler (UCM)
Unlabeled Consensus Modeler (UCM) is a Machine Learning method that models a voting ensemble's consensus in the unlabeled space. Data points are pseudo-labeled based on the ensemble's consensus. If consensus is not reached, the point is marked as "Unknown," which is treated as a new, additional class. A meta model is trained upon this half-labeled set. The predictions of the meta model are combined with those of the ensemble to produce the final predictions.

## Software diagram
<img  src="ucm-software-diagram.png"  width="400"/>

For a description of the diagram and how it corresponds to the structure of this repository, please see this [demo video](https://www.youtube.com/watch?v=seAAI6IZaPs).

## Dependencies
```
python          3.6
pandas          0.25.3
numpy           1.17.3
scikit-learn    0.22.1
```  

## How to use
To use UCM, you create an instance of the `UcmPipeline` class, whose constructor takes two mandatory arguments: a vote aggregator and a meta model. You can use my `MajorityVoting` for the vote aggregator and Scikit-learn's [`KNeighborsClassifier`](https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsClassifier.html) for the meta model. A requirement for these two components is that they need to have the `predict_proba()` method implemented.
```python
ucm = UcmPipeline(vote_aggregator, meta_model)
ucm.fit(X_train, y_train)
predictions = ucm.predict(X_test)
```
I included a detailed example on how to use the code in `ucm_pipeline.py`. Here is a [video](https://www.youtube.com/watch?v=upsGUZ0qqG4) explaining the code in finer details. For other components in the diagram, there is an example on how to use the class at the end of the file corresponding to each of the components.
 
While most of the code files are "object-oriented," there are two scripts: `clean.py` and `evaluate.py`. Both take in a `.json` file that has the metadata about a collection of datasets. `clean.py` can be used to clean the raw datasets listed in `raw-metadata.json`. The clean datasets can then be sent to `datasets/clean/`, which contains another metadata file: `clean-metadata.json`. This is inputted to `evaluate.py`, which runs the entire pipeline and returns the performance of Majority Voting, UCM, and Weighted Majority Voting on the dataset(s) specified. On how to pass arguments to these two scripts, enter the following command:
```
$ python3 <script.py> --help
```
