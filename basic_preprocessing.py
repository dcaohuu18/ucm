import numpy as np 
import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.utils.validation import check_is_fitted
from sklearn.preprocessing import OneHotEncoder, StandardScaler
from sklearn.impute import SimpleImputer
from sklearn.pipeline import Pipeline 

 
class BasicPreprocessing( BaseEstimator, TransformerMixin ):    
    def fit(self, X, y = None, category_fea_idxes='inferred'):
        self.n_features_ = X.shape[1]
        self.X_ = X

        # Determine the categorical and numerical idxes
        if category_fea_idxes == 'inferred':
            self.category_fea_idxes = []
            self.numeric_fea_idxes = []
            for i in range(self.n_features_):
                try:
                    self.X_[:, i] = self.X_[:, i].astype(float)
                    self.numeric_fea_idxes.append(i)
                except ValueError:
                    self.category_fea_idxes.append(i)

        elif ( category_fea_idxes != [] and 
               (min(category_fea_idxes)<0 or max(category_fea_idxes)>self.n_features_-1) ):
            raise IndexError('Given category feature indexes are out of range '
                             'of the input X')
        
        else:
            unique_cat_idxes = set(category_fea_idxes)
            self.category_fea_idxes = list(unique_cat_idxes)
            self.numeric_fea_idxes = [i for i in range(self.n_features_) 
                                      if i not in unique_cat_idxes]

        # Fit the transformers:
        if self.numeric_fea_idxes != []:
            self.imp_mean = SimpleImputer(missing_values=np.nan, strategy='mean')
            self.X_[:, self.numeric_fea_idxes] = self.imp_mean.fit_transform(
                                                    self.X_[:, self.numeric_fea_idxes])

            self.scaler = StandardScaler()
            self.scaler.fit(self.X_[:, self.numeric_fea_idxes])

        if self.category_fea_idxes != []:
            self.imp_mode = SimpleImputer(missing_values=np.nan, strategy='most_frequent')
            self.X_[:, self.category_fea_idxes] = self.imp_mode.fit_transform(
                                                    self.X_[:, self.category_fea_idxes])

            self.encoder = OneHotEncoder()
            self.encoder.fit(self.X_[:, self.category_fea_idxes])
         
        return self 
    
    def transform(self, X, y = None):
        # Check is fit had been called
        check_is_fitted(self, 'n_features_')

        # Check that the input is of the same shape as the one passed during fit.
        if X.shape[1] != self.n_features_:
            raise ValueError('Shape of input is different from what was seen '
                             'in `fit`')

        if self.numeric_fea_idxes != []:
            X[:, self.numeric_fea_idxes] = self.imp_mean.transform(X[:, self.numeric_fea_idxes])
            X[:, self.numeric_fea_idxes] = self.scaler.transform(X[:, self.numeric_fea_idxes])

        if self.category_fea_idxes != []:
            X[:, self.category_fea_idxes] = self.imp_mode.transform(X[:, self.category_fea_idxes])
            encoded_cols = self.encoder.transform(X[:, self.category_fea_idxes]).toarray()
            X = np.hstack((X, encoded_cols))

        # dropping the original categorical columns
        # now that they are already encoded
        X = np.delete(X, self.category_fea_idxes, 1)

        return X


if __name__ == '__main__':
    preprocessor = BasicPreprocessing()
    df = pd.read_csv('/eccs/home/dcaohuu18/ucm/datasets/raw/abalone.data') 
    X_t = preprocessor.fit_transform(df.values, category_fea_idxes=[0])
    print(X_t)