import pandas as pd
import numpy as np
from sklearn.preprocessing import LabelEncoder
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.base import clone
from sklearn import metrics
from sklearn.model_selection import cross_val_score
from basic_preprocessing import BasicPreprocessing
from majority_voting import MajorityVoting
from ucm_pipeline import UcmPipeline
import argparse
import json
import os


def evaluate_one(dataset_fn, dataset_info_dict):
    df = pd.read_csv(dataset_fn).to_numpy()

    target_idx = dataset_info_dict['target_col_idx']
    y = LabelEncoder().fit_transform(df[:, target_idx])

    cat_fea_idxes = dataset_info_dict['category_fea_idxes']
    X = BasicPreprocessing().fit_transform(np.delete(df, target_idx, 1),
                                           category_fea_idxes=cat_fea_idxes)

    clf1 = LogisticRegression(solver='lbfgs', multi_class ='auto', max_iter=1000)
    clf2 = KNeighborsClassifier(n_neighbors=3, weights='distance')
    clf3 = SVC(random_state=1)
    clf4 = GaussianNB()
    clf5 = DecisionTreeClassifier(random_state=1)
    clf6 = MLPClassifier(hidden_layer_sizes=((X.shape[1]+1)//2), max_iter=5000, random_state=1)    

    major_voting = MajorityVoting(estimators = [clf1, clf2, clf3, clf4, clf5, clf6])
    meta_knn = KNeighborsClassifier(n_neighbors=3, weights='distance')
    ucm = UcmPipeline(major_voting, meta_knn)

    weighted_mv = MajorityVoting(estimators = [clf1, clf2, clone(clf2), clf3, clf4, clf5, clf6])

    mv_acc = cross_val_score(major_voting, X, y, cv=10, scoring='accuracy').mean()
    ucm_acc = cross_val_score(ucm, X, y, cv=10, scoring='accuracy').mean()
    wmv_acc = cross_val_score(weighted_mv, X, y, cv=10, scoring='accuracy').mean()

    mv_f1 = cross_val_score(major_voting, X, y, cv=10, scoring='f1_weighted').mean()
    ucm_f1 = cross_val_score(ucm, X, y, cv=10, scoring='f1_weighted').mean()
    wmv_f1 = cross_val_score(weighted_mv, X, y, cv=10, scoring='f1_weighted').mean()

    return mv_acc, ucm_acc, wmv_acc, mv_f1, ucm_f1, wmv_f1


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-js", "--meta_json", help = "The metadata json.", type = str, required=True)
    parser.add_argument("-k", "--key", 
                        help = "The key of the dataset to be evaluated on. "
                               "To use all datasets in the metadata json, enter 'all'.", 
                        type = str, required=True)
    parser.add_argument("-to", "--to_fn",
                        help = "The file the performance scores will be outputted to. "
                               "If not specified, the scores will be printed onto the screen.", 
                        type = str)
    args = parser.parse_args()

    with open(args.meta_json) as meta_file:
        all_info_dict = json.load(meta_file)

    scores_list = []

    if args.key == 'all':
        for dataset_fn, dataset_info_dict in all_info_dict.items():
            try:
                mv_acc, ucm_acc, wmv_acc, mv_f1, ucm_f1, wmv_f1 = evaluate_one(dataset_fn, dataset_info_dict)
                base_dataset_fn = os.path.basename(dataset_fn)
                scores_list.append([base_dataset_fn, mv_acc, ucm_acc, wmv_acc, mv_f1, ucm_f1, wmv_f1])
            except Exception as e:
                print(e)
    else:
        dataset_fn = args.key
        dataset_info_dict = all_info_dict[args.key]
        mv_acc, ucm_acc, wmv_acc, mv_f1, ucm_f1, wmv_f1 = evaluate_one(dataset_fn, dataset_info_dict)
        base_dataset_fn = os.path.basename(dataset_fn)
        scores_list.append([base_dataset_fn, mv_acc, ucm_acc, wmv_acc, mv_f1, ucm_f1, wmv_f1])

    scores_df = pd.DataFrame(scores_list, columns=['dataset', 'mv_acc', 'ucm_acc', 'wmv_acc', 
                                                   'mv_f1', 'ucm_f1', 'wmv_f1'])

    if args.to_fn == None:
        print(scores_df)
    else:
        scores_df.to_csv(args.to_fn, index=False)


if __name__ == '__main__':
    main()