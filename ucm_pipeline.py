import numpy as np
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from sklearn.utils.multiclass import unique_labels
from pseudo_labeling import PseudoLabeling


class UcmPipeline(ClassifierMixin, BaseEstimator):
    def __init__(self, vote_aggregator, meta_model, certain_thresh=0.51):
        self.vote_aggregator = vote_aggregator
        self.meta_model = meta_model
        self.certain_thresh = certain_thresh

    def fit(self, X, y):
        # Check that X and y have correct shape
        X, y = check_X_y(X, y)
        self.X_ = X
        self.y_ = y

        # Fit the vote aggregator
        self.vote_aggregator.fit(self.X_, self.y_)
        self.classes_ = self.vote_aggregator.classes_

        return self

    def predict_proba(self, X):
        # Check is fit had been called
        check_is_fitted(self, ['X_', 'y_'])

        # Input validation
        X = check_array(X)

        voting_prob_preds = self.vote_aggregator.predict_proba(X)
        voting_classes = self.vote_aggregator.classes_
        self.classes_ = voting_classes

        self.prob_predictions = voting_prob_preds

        pseudo_labeler = PseudoLabeling(voting_classes, self.certain_thresh)
        pseudo_y = pseudo_labeler.fit_transform(voting_prob_preds)

        unknown_row_idxes = np.where(pseudo_y==pseudo_labeler.class_unknown_)[0]
        if unknown_row_idxes.size == 0:
            return self.prob_predictions

        for unknown_i in unknown_row_idxes:
            # Fit on all the other samples except the current one:
            self.meta_model.fit(np.delete(X, unknown_i, axis=0), 
                                np.delete(pseudo_y, unknown_i))
            meta_prob_preds = self.meta_model.predict_proba(X[unknown_i].reshape(1, -1))[0]
            meta_classes = self.meta_model.classes_

            # add the two probability predictions
            # assume the two lists of classes are sorted:                
            self.prob_predictions[unknown_i][np.in1d(voting_classes, meta_classes, 
                                                     assume_unique=True)
                                            ] += meta_prob_preds[np.in1d(meta_classes, voting_classes, 
                                                                         assume_unique=True)]
        return self.prob_predictions

    def predict(self, X):
        self.predict_proba(X)
        return self.classes_[np.argmax(self.prob_predictions, axis=1)]


if __name__ == '__main__':
    from sklearn.linear_model import LogisticRegression
    from sklearn.naive_bayes import GaussianNB
    from sklearn.ensemble import RandomForestClassifier
    from sklearn.neighbors import KNeighborsClassifier
    from majority_voting import MajorityVoting

    X = np.array([[-1, -1], [-2, -1], [-3, -2], [1, 1], [2, 1], [3, 2]])
    y = np.array([1, 0, 1, 2, 2, 2])
    
    clf1 = LogisticRegression(solver='lbfgs', multi_class = 'auto')
    clf2 = RandomForestClassifier(n_estimators=50, random_state=1)
    clf3 = GaussianNB()

    major_voting = MajorityVoting(estimators = [clf1, clf2, clf3])
    meta_knn = KNeighborsClassifier(n_neighbors=5, weights ='distance')

    ucm = UcmPipeline(major_voting, meta_knn)
    ucm.fit(X, y)
    print(ucm.predict(X))