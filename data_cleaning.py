import numpy as np
import pandas as pd


class DataCleaning:
    def __init__(self, data_fn, delimiter=',', header=None):
        self.df = pd.read_csv(data_fn, delimiter=delimiter, header=header, 
                              dtype=str, engine='python')

    def missing_to_nan(self, missing_inds):
        self.df.replace(missing_inds, np.nan, inplace=True)

    def drop_cols_at(self, col_idxes):
        self.df.drop(self.df.columns[col_idxes], axis=1, inplace=True)
    
    def drop_rows_at(self, row_idxes):
        self.df.drop(row_idxes, axis=0, inplace=True)

    def drop_constant_cols(self):
        self.df = self.df.loc[:, self.df.nunique() > 1]

    def output_to_csv(self, output_fn, header=False):
        self.df.to_csv(output_fn, header=header, index=False)


if __name__ == '__main__':
    cleaner = DataCleaning('datasets/raw/anneal.data')
    cleaner.missing_to_nan('?')
    cleaner.drop_constant_cols() 