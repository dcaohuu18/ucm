from data_cleaning import DataCleaning
import argparse
import json
import os


def clean_one(dataset_fn, dataset_info_dict, to_dir):
    cleaner = DataCleaning(dataset_fn, delimiter=dataset_info_dict['delimiter']) # HEADER?

    try:
        dup_row_idxes = dataset_info_dict['duplicated_row_idxes']
        cleaner.drop_rows_at(dup_row_idxes)
    except KeyError:
        pass
    
    try:
        missing_inds = dataset_info_dict['missing_vals']
        cleaner.missing_to_nan(missing_inds)
    except KeyError:
        pass

    try:
        redudant_col_idxes = dataset_info_dict['redundant_col_idxes']
        cleaner.drop_cols_at(redudant_col_idxes)
    except KeyError:
        pass

    cleaner.drop_constant_cols()
    
    base_fn = os.path.basename(dataset_fn)
    cleaner.output_to_csv(f"{to_dir}{base_fn}.clean")
    

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-js", "--meta_json", help = "The metadata json.", type = str, required=True)
    parser.add_argument("-k", "--key", 
                        help = "The key of the dataset to be cleaned. "
                               "To clean all datasets in the metadata json, enter 'all'.", 
                        type = str, required=True)
    parser.add_argument("-to", "--to_dir",
                        help = "The directory the cleaned data will be outputted to. "
                               "If not specified, it'll be outputted to the working directory.", 
                        type = str)
    args = parser.parse_args()

    with open(args.meta_json) as meta_file:
        all_info_dict = json.load(meta_file)

    to_dir = args.to_dir if args.to_dir!=None else ''

    if args.key == 'all':
        for dataset_fn, dataset_info_dict in all_info_dict.items():
            clean_one(dataset_fn, dataset_info_dict, to_dir)
    else:
        dataset_fn = args.key
        dataset_info_dict = all_info_dict[args.key]
        clean_one(dataset_fn, dataset_info_dict, to_dir)


if __name__ == '__main__':
    main()